# Third Party Software

This project uses 3rd party software:

* Part of the [gitlab-terraform.sh](https://gitlab.com/gitlab-org/terraform-images/-/blob/master/src/bin/gitlab-terraform.sh) script
  from project [gitlab-org/terraform-images](https://gitlab.com/gitlab-org/terraform-images),
  distributed under the terms of the [MIT License](https://gitlab.com/gitlab-org/terraform-images/-/blob/master/LICENSE)
* Part of [Terraform.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Terraform.gitlab-ci.yml)
  from project [gitlab-org/gitlab-foss](https://gitlab.com/gitlab-org/gitlab-foss)
  distributed under the terms of the [MIT Expat license](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/LICENSE)
